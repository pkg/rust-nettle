//! Rust bindings for the [Nettle cryptographic
//! library](https://www.lysator.liu.se/~nisse/nettle/).

#![warn(missing_docs)]

// Allow Struct { field: field }
#![allow(clippy::redundant_field_names)]
// Allow module name repetition, e.g. modname::modname
#![allow(clippy::module_inception)]

extern crate libc;
extern crate nettle_sys;
extern crate getrandom;

mod errors;
pub use crate::errors::{Error, Result};

mod helper;

pub mod hash;
pub mod cipher;
pub mod mode;
pub mod aead;
pub mod mac;
pub mod kdf;
pub mod rsa;

pub mod random;

pub mod ecc;

pub mod curve25519;
pub mod curve448;
pub mod dsa;
pub mod ecdh;
pub mod ecdsa;
pub mod ed25519;
pub mod ed448;
